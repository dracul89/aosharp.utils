﻿using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.Misc;
using AOSharp.Core.UI;
using GraceBot9001.States;
using GraceBot9001.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Serilog;
using AOSharp.Core.IPC;
using GraceBot9001.IPCMessages;
using SmokeLounge.AOtomation.Messaging.Messages;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using SmokeLounge.AOtomation.Messaging.GameData;
using AOSharp.Common.GameData;

namespace GraceBot9001
{
    public class Main : AOPluginEntry
    {
        internal const long GMI_MAX_USABLE_CREDITS = 3999999999;
        internal const int PORTABLE_BANK_ID = 288762;
        internal const int SHOP_FOOD_ID = 225972;
        internal const string PUMP_ITEM_NAME = "GRACE";

        internal static IPCChannel IPCChannel;
        internal static StateMachine StateMachine;
        internal static bool IsGMIChar = false;
        internal static long DesiredGracePrice = 500000000000;
        internal static long QueuedCash = 0;

        private Dictionary<State, Action> _stateHandlers = new Dictionary<State, Action>()
        {
            { State.PreflightChecks, PreflightChecksState.Run },
            { State.OpeningBank, OpeningBankState.Run },
            { State.SpawnCredits, SpawnCreditsState.Run },
            { State.ResettingDupeBags, ResettingDupeBagsState.Run },
        };

        public override void Run(string pluginDir)
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.ChatSink()
                .CreateLogger();

            StateMachine = new StateMachine();

            SetupChatCommands();
            SetupIPC();

            Network.N3MessageReceived += OnN3Message;
            Game.OnUpdate += OnUpdate;
        }

        private void OnN3Message(object sender, N3Message n3Msg)
        {
            if (n3Msg.N3MessageType == N3MessageType.Trade && StateMachine.State == State.SpawnCredits)
            {
                TradeMessage tradeMsg = (TradeMessage)n3Msg;

                if (tradeMsg.Action == TradeAction.Open)
                {
                    if ((IdentityType)tradeMsg.Param1 == IdentityType.VendingMachine)
                        SpawnCreditsState.TradeOpened();
                    else
                        Trade.Decline();
                }
            }
            else if (n3Msg.N3MessageType == N3MessageType.MarketSend && StateMachine.State == State.SpawnCredits)
            {
                SpawnCreditsState.OnDepositSent();
            }
        }

        private void OnUpdate(object sender, float e)
        {
            try
            {
                if (_stateHandlers.TryGetValue(StateMachine.State, out Action action))
                    action.Invoke();
            }
            catch (Exception ex)
            {
                FatalError(ex.Message);
            }
        }

        private void SetupChatCommands()
        {
            Chat.RegisterCommand("autograce", (string command, string[] param, ChatWindow chatWindow) =>
            {
                if (param.Length < 1)
                    return;

                switch (param[0])
                {
                    case "on":
                        Log.Information("Auto-Grace enabled.");
                        IsGMIChar = true;
                        StateMachine.Fire(Trigger.Enabled);
                        IPCChannel.Broadcast(new StartMessage());
                        break;
                    case "off":
                        Log.Information("Auto-Grace disabled.");
                        IsGMIChar = false;
                        StateMachine.Fire(Trigger.Disabled);
                        break;
                }
            });
        }

        private void SetupIPC()
        {
            IPCChannel = new IPCChannel(123);

            IPCChannel.RegisterCallback((int)IPCOpcode.Start, (sender, msg) =>
            {
                StateMachine.Fire(Trigger.Enabled);
            });

            IPCChannel.RegisterCallback((int)IPCOpcode.DupeItemsReady, (sender, msg) =>
            {
                StateMachine.Fire(Trigger.DupeItemsReady);
            });

            IPCChannel.RegisterCallback((int)IPCOpcode.ResetDupeBags, (sender, msg) =>
            {
                StateMachine.Fire(Trigger.ResetDupeBags);
            });

            IPCChannel.RegisterCallback((int)IPCOpcode.Errored, (sender, msg) =>
            {
                Log.Error("Got errored message from other char.");
                StateMachine.Fire(Trigger.Error);
            });
        }

        internal static void FatalError(string message)
        {
            Log.Error(message);
            StateMachine.Fire(Trigger.Error);
            IPCChannel.Broadcast(new ErroredMessage());
        }
    }
}
