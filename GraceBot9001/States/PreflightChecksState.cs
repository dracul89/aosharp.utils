﻿using AOSharp.Core.GMI;
using AOSharp.Core.Inventory;
using AOSharp.Core.Misc;
using AOSharp.Core.UI;
using Serilog;
using System.Threading.Tasks;

namespace GraceBot9001.States
{
    internal static class PreflightChecksState
    {
        internal static void Run()
        {
            if (!Inventory.Bank.IsOpen)
            {
                Main.StateMachine.Fire(Trigger.BankNotOpen);
                return;
            }

            if(!Inventory.Bank.FindContainer("DupeBag", out _))
            {
                Main.FatalError($"Unable to locate bag named \"DupeBag\" within your bank.");
                return;
            }

            if(!Main.IsGMIChar && !Inventory.FindContainer("ShopFood", out _))
            {
                Main.FatalError($"Unable to locate bag named \"ShopFood\".");
                return;
            }

            if (Main.IsGMIChar && !Inventory.Find(300636, out _))
            {
                Main.FatalError($"Unable to locate Nano Can: Summon Buckethead Technodealer.");
                return;
            }

            if (Main.IsGMIChar && Inventory.Find(Main.SHOP_FOOD_ID, out _))
            {
                Main.FatalError($"Must not have the shop food in your inventory to start.");
                return;
            }

            Main.StateMachine.Fire(Trigger.PreflightChecksPassed);
        }
    }
}
