﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.GMI;
using AOSharp.Core.Inventory;
using AOSharp.Core.Misc;
using AOSharp.Core.UI;
using GraceBot9001.IPCMessages;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraceBot9001.States
{
    internal static class SpawnCreditsState
    {
        private const long MAX_QUEUED_CASH = 3500000000;

        private static bool WaitingOnDeposit = false;
        private static bool DupeBagsReady = false;
        private static AutoResetInterval _interval = new AutoResetInterval(500);

        internal static void Entry()
        {
            Trade.Decline();
            WaitingOnDeposit = false;
            DupeBagsReady = false;

            if (Inventory.Bank.FindContainer("DupeBag", out Backpack db))
            {
                Item.MoveItemToInventory(db.Slot);
                return;
            }
        }

        internal static void Run()
        {
            if (!_interval.Elapsed)
                return;

            if (WaitingOnDeposit)
                return;

            int cash = DynelManager.LocalPlayer.GetStat(Stat.Cash);

            if (cash > 0)
            {
                int cashToDeposit = (int)Math.Min(MAX_QUEUED_CASH - Main.QueuedCash, cash);

                GMI.Deposit(cashToDeposit);
                Main.QueuedCash += cashToDeposit;
                WaitingOnDeposit = true;

                Log.Debug($"Depositing {cashToDeposit}. My Cash: {cash}. Queued Cash: {Main.QueuedCash}.");

                if (Main.QueuedCash >= MAX_QUEUED_CASH)
                    Main.StateMachine.Fire(Trigger.CreditsQueued);

                return;
            }

            if(Inventory.FindContainer("DupeBag", out Backpack dupeBag))
            {
                if (!dupeBag.IsOpen)
                {
                    Item.Use(dupeBag.Slot);
                    Log.Debug("Opening dupe bag.");
                    return;
                }

                if(!dupeBag.Find(Main.SHOP_FOOD_ID, out _))
                {
                    Log.Debug("Can't find shop food items in dupe bag.");
                    Item.ContainerAddItem(dupeBag.Slot, Inventory.Bank.Identity);
                    Main.StateMachine.Fire(Trigger.NeedShopFood);
                    return;
                }

                DupeBagsReady = true;
            }

            if (!DupeBagsReady)
                return;

            if (DynelManager.LocalPlayer.Buffs.Find(300670, out Buff bucketheadLockout))
            {
                if (bucketheadLockout.RemainingTime < 30)
                    return; 
            }

            if (DynelManager.Find("Buckethead Technodealer", out SimpleItem buckethead))
            {
                //Log.Debug("Opening trade with buckethead.");
                buckethead.Use();
            }
            else
            {
                if (Inventory.Find(300636, out Item bucketHeadNanoCan))
                    bucketHeadNanoCan.Use();
            }
        }

        internal static void TradeOpened()
        {
            Log.Debug("Trade Opened.");

            if (Inventory.FindContainer("DupeBag", out Backpack dupeBag) && dupeBag.Find(Main.SHOP_FOOD_ID, out Item shopFood))
            {
                Log.Debug("Selling shop food.");

                shopFood.MoveToInventory();

                for (int i = 0x40; i < 0x64; i++)
                {
                    Identity slot = new Identity(IdentityType.Inventory, i);
                    if (!Inventory.Find(slot, out Item _))
                    {
                        Trade.AddItem(DynelManager.LocalPlayer.Identity, slot);
                        break;
                    }
                }

                Trade.Accept(Identity.None);
            }
            else
            {
                Log.Debug("Can't find shop food items in dupe bag.");
            }
        }

        internal static void OnDepositSent()
        {
            WaitingOnDeposit = false;
        }
    }
}
