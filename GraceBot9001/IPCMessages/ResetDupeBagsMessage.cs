﻿using AOSharp.Core.IPC;
using SmokeLounge.AOtomation.Messaging.Serialization.MappingAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraceBot9001.IPCMessages
{
    [AoContract((int)IPCOpcode.ResetDupeBags)]
    public class ResetDupeBagsMessage : IPCMessage
    {
        public override short Opcode => (int)IPCOpcode.ResetDupeBags;
    }
}
