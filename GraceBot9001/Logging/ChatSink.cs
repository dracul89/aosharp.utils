﻿using AOSharp.Common.GameData;
using AOSharp.Core.UI;
using Serilog;
using Serilog.Configuration;
using Serilog.Core;
using Serilog.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace GraceBot9001.Logging
{
    public static class ChatSinkExtensions
    {
        public static LoggerConfiguration ChatSink(this LoggerSinkConfiguration loggerConfiguration, IFormatProvider fmtProvider = null)
        {
            return loggerConfiguration.Sink(new ChatSink(fmtProvider));
        }
    }

    public class ChatSink : ILogEventSink
    {
        IFormatProvider _formatProvider;

        public ChatSink(IFormatProvider formatProvider)
        {
            _formatProvider = formatProvider;
        }

        public void Emit(LogEvent logEvent)
        {
            string message = $"[{Assembly.GetExecutingAssembly().GetName().Name}] {logEvent.RenderMessage(_formatProvider)}";

            if (logEvent.Level == LogEventLevel.Debug)
                Chat.WriteLine(message, ChatColor.Yellow);
            else if (logEvent.Level == LogEventLevel.Error)
                Chat.WriteLine(message, ChatColor.Red);
            else if (logEvent.Level == LogEventLevel.Warning)
                Chat.WriteLine(message, ChatColor.Orange);
            else if (logEvent.Level == LogEventLevel.Information)
                Chat.WriteLine(message, ChatColor.White);
        }
    }
}
