﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.UI;
using GraceBot9001.IPCMessages;
using GraceBot9001.States;
using Serilog;
using Stateless;
using System;

namespace GraceBot9001
{
    internal enum Trigger
    {
        Enabled,
        Disabled,
        Error,
        BankNotOpen,
        BankOpened,
        PreflightChecksPassed,
        NeedCredits,
        NeedShopFood,
        CreditsQueued,
        ResetDupeBags,
        DupeBagsReset,
        DupeItemsReady
    }

    internal enum State
    {
        Idle,
        Errored,
        PreflightChecks,
        OpeningBank,
        WaitingOnOrders,
        PumpingOrders,
        SpawnCredits,
        Duping,
        ResettingDupeBags
    }

    internal class StateMachine : StateMachine<State, Trigger>
    {
        public StateMachine() : base(State.Idle)
        {
            OnTransitioned(OnTransitionAction);
            InitializeStateMachine();
        }

        private void OnTransitionAction(Transition obj)
        {
            Log.Debug($"State transition from {obj.Source} to {obj.Destination} triggered by {obj.Trigger}. Re-entry is {obj.IsReentry}");
        }

        private void GlobalPermit(Trigger trigger, State destinationState)
        {
            foreach (State state in Enum.GetValues(typeof(State)))
            {
                if (state.Equals(destinationState))
                    Configure(state).Ignore(trigger);
                else
                    Configure(state).Permit(trigger, destinationState);
            }
        }

        private void InitializeStateMachine()
        {
            Configure(State.Errored)
                .Permit(Trigger.Enabled, State.PreflightChecks);

            Configure(State.Idle)
                .Permit(Trigger.Enabled, State.PreflightChecks);

            Configure(State.PreflightChecks)
                .Permit(Trigger.BankNotOpen, State.OpeningBank)
                .PermitIf(Trigger.PreflightChecksPassed, State.PumpingOrders, () => Main.IsGMIChar)
                .PermitIf(Trigger.PreflightChecksPassed, State.WaitingOnOrders, () => !Main.IsGMIChar);

            Configure(State.OpeningBank)
                .Permit(Trigger.BankOpened, State.PreflightChecks);

            Configure(State.PumpingOrders)
                .OnEntry(PumpingOrdersState.Entry)
                .Permit(Trigger.NeedCredits, State.SpawnCredits);

            Configure(State.SpawnCredits)
                .OnEntry(SpawnCreditsState.Entry)
                .Permit(Trigger.NeedShopFood, State.Duping)
                .Permit(Trigger.CreditsQueued, State.PumpingOrders);

            Configure(State.Duping)
                .OnEntry(() => Main.IPCChannel.Broadcast(new ResetDupeBagsMessage()))
                .Permit(Trigger.DupeItemsReady, State.SpawnCredits);

            Configure(State.WaitingOnOrders)
                .Permit(Trigger.ResetDupeBags, State.ResettingDupeBags);

            Configure(State.ResettingDupeBags)
                .Permit(Trigger.DupeBagsReset, State.WaitingOnOrders);

            GlobalPermit(Trigger.Error, State.Errored);
            GlobalPermit(Trigger.Disabled, State.Idle);
        }
    }
}
