﻿using AOSharp.Common.GameData;
using AOSharp.Common.Helpers;
using AOSharp.Common.Unmanaged.Imports;
using AOSharp.Core;
using AOSharp.Core.GMI;
using AOSharp.Core.Inventory;
using AOSharp.Core.Misc;
using AOSharp.Core.UI;
using SmokeLounge.AOtomation.Messaging.Messages;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace GraceBot9000
{
    public class Main : AOPluginEntry
    {
        internal const long GMI_MAX_USABLE_CREDITS = 3999999999;

        private long _graceOrderPriceLimit = 100000000000;
        private bool _autoGrace = false;
        private AutoResetInterval _autoGraceInterval = new AutoResetInterval(10000);

        public override void Run(string pluginDir)
        {
            Chat.RegisterCommand("spawncredits", (string command, string[] param, ChatWindow chatWindow) =>
            {
                if (param.Length < 1)
                    return;

                int itemId;
                if (!int.TryParse(param[0], out itemId))
                {
                    Chat.WriteLine($"{param[0]} is not a valid item id.");
                    return;
                }

                if (Inventory.NumFreeSlots < 1)
                {
                    Chat.WriteLine("You need at least one available inventory slot.");
                    return;
                }

                if (Targeting.Target == null || !(Targeting.Target.Identity.Type == IdentityType.VendingMachine || Targeting.Target.Name == "Buckethead Technodealer"))
                {
                    Chat.WriteLine("You must target a shop.");
                    return;
                }

                if (DynelManager.LocalPlayer.DistanceFrom(Targeting.Target) > 5)
                {
                    Chat.WriteLine("You are too far away from the shop.");
                    return;
                }

                if(Inventory.Find(itemId, out Item item))
                {
                    float shopModifier = (Targeting.Target.Name == "Buckethead Technodealer" ? 4 : Targeting.Target.GetStat(Stat.BuyModifier)) / 4f;
                    int clModifier = DynelManager.LocalPlayer.GetStat(Stat.ComputerLiteracy) / 40;
                    int value = item.GetStat(Stat.Value);
                    float sellPrice = (value * shopModifier * (100 + clModifier) / 2500);

                    int quantityToSell = (int)((1000000000 - DynelManager.LocalPlayer.GetStat(Stat.Cash)) / sellPrice);

                    if (quantityToSell == 0)
                        return;

                    Chat.WriteLine($"Selling {quantityToSell} {item.Name} @ {sellPrice:N0} each");

                    Targeting.Target.Cast<SimpleItem>().Use();
                    item.Split(quantityToSell);

                    for (int i = 0x40; i < 0x64; i++)
                    {
                        Identity slot = new Identity(IdentityType.Inventory, i);
                        if (!Inventory.Find(slot, out Item _))
                        {
                            Trade.AddItem(DynelManager.LocalPlayer.Identity, slot);
                            break;
                        }
                    }

                    Trade.Accept(Identity.None);
                }
            });

            Chat.RegisterCommand("gracepricelimit", (string command, string[] param, ChatWindow chatWindow) =>
            {
                if (param.Length < 1)
                    return;

                if(long.TryParse(param[0], out _graceOrderPriceLimit))
                    Chat.WriteLine($"Grace order price limit set to {_graceOrderPriceLimit:N0}.");
            });

            Chat.RegisterCommand("autograce", (string command, string[] param, ChatWindow chatWindow) =>
            {
                if (param.Length < 1)
                    return;

                switch (param[0])
                {
                    case "on":
                        Chat.WriteLine("Auto-Grace enabled.");
                        _autoGrace = true;
                        break;
                    case "off":
                        Chat.WriteLine("Auto-Grace disabled.");
                        _autoGrace = false;
                        break;
                }
            });

            Chat.RegisterCommand("unlockgmi", (string command, string[] param, ChatWindow chatWindow) =>
            {
                RequestGMIInventory(OnMarketInventoryLoaded_Withdraw);
            });

            Chat.RegisterCommand("deposit", (string command, string[] param, ChatWindow chatWindow) =>
            {
                GMI.Deposit(DynelManager.LocalPlayer.GetStat(Stat.Cash));
            });

            Game.OnUpdate += OnUpdate;
        }

        private void OnUpdate(object sender, float e)
        {
            if (_autoGrace && _autoGraceInterval.Elapsed)
                RequestGMIInventory(OnMarketInventoryLoaded_Grace);
        }

        private void RequestGMIInventory(Action<MarketInventory> callback)
        {
            GMI.GetInventory().ContinueWith(marketInventory =>
            {
                Chat.WriteLine(marketInventory.IsFaulted);
                Chat.WriteLine(marketInventory.Result.Credits);

                if (!marketInventory.IsFaulted && marketInventory.Result != null)
                    callback(marketInventory.Result);
            });
        }

        private void OnMarketInventoryLoaded_Withdraw(MarketInventory marketInventory)
        {
            if (marketInventory.Credits <= GMI_MAX_USABLE_CREDITS)
                return;

            long cashToWithdraw = Math.Min(marketInventory.Credits - GMI_MAX_USABLE_CREDITS, 999999999);
            GMI.WithdrawCash(cashToWithdraw).ContinueWith(withdrawCash =>
             {
                 if (withdrawCash.Result.Succeeded)
                 {
                     Chat.WriteLine($"Withdrew {cashToWithdraw:N0}. New Balance: {marketInventory.Credits - cashToWithdraw:N0}");
                     RequestGMIInventory(OnMarketInventoryLoaded_Withdraw);
                 }
                 else
                 {
                     Chat.WriteLine($"Failed to withdraw cash. The error is {withdrawCash.Result.Message}");
                 }
             });
        }

        private void OnMarketInventoryLoaded_Grace(MarketInventory marketInventory)
        {
            if (marketInventory.Credits < 100000 || marketInventory.Credits > GMI_MAX_USABLE_CREDITS)
                return;

            GMI.GetMarketOrders().ContinueWith(marketOrders =>
            {
                if (!marketOrders.IsFaulted && marketOrders.Result != null)
                    OnMarketOrdersLoaded(marketInventory, marketOrders.Result);
            });
        }

        private void OnMarketOrdersLoaded(MarketInventory marketInventory, MyMarketOrders myOrders)
        {
            MyMarketBuyOrder graceOrder = myOrders.BuyOrders.Where(x => x.Name == "GRACE" && x.Price < _graceOrderPriceLimit).OrderByDescending(x => x.Price).FirstOrDefault();

            if (graceOrder == null)
                return;

            long desiredIncrease = Math.Min(_graceOrderPriceLimit - graceOrder.Price, (long)(marketInventory.Credits * 0.995f)) / graceOrder.Count;

            Chat.WriteLine($"Increasing Grace order {graceOrder.Id} by {desiredIncrease:N0}.");

            graceOrder.ModifyPrice(graceOrder.Price + desiredIncrease).ContinueWith(modifyOrder =>
            {
                if (modifyOrder.Result.Succeeded)
                    Chat.WriteLine($"Grace order {graceOrder.Id} successfully increased to {(graceOrder.Price + desiredIncrease):N0}");
                else
                    Chat.WriteLine($"Failed to modify order {graceOrder.Id}. The error is {modifyOrder.Result.Message}");
            });
        }
    }
}
