﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TradeskillLeveler
{
    internal class ItemCache
    {
        internal TradeskillPair Ql1TradeskillPair => new TradeskillPair(Ql1SourceItem, MinTradeskillItem);
        internal TradeskillPair HighestTradeskillPair => new TradeskillPair(HackerTool, HighestItem());
        internal Item MinTradeskillItem = null;
        internal Item LowTradeskillItem = null;
        internal Item HighTradeskillItem = null;
        internal Item Ql1SourceItem = null;
        internal Item HackerTool = null;

        internal void LoadItems()
        {

            if (!FindGrafts(out LowTradeskillItem, out HighTradeskillItem))
                throw new MissingItemException("Unable to locate any equipped grafts. Expected low ql Symbio-Graft: Drain Sense or ql 200 Boosted-Graft: Feet of Stone.");

            if (!FindHackerTool(out HackerTool))
                throw new MissingItemException("Expected Ql 150+ Hacker Tool in inventory.");

            if (DynelManager.LocalPlayer.Profession == Profession.Shade)
            {
                if (!FindLockPick(out Ql1SourceItem))
                    throw new MissingItemException("Expected Lock Pick in inventory.");

                if (!FindNCU(out MinTradeskillItem))
                    throw new MissingItemException("You must have a ql 1 NCU equipped");
            }
            else
            {
                if (!FindImplantDisassemblyClinic(out Ql1SourceItem))
                    throw new MissingItemException("Expected Ql 1+ Implant Disassembly Clinic in inventory.");

                if (!FindImplant(out MinTradeskillItem))
                    throw new MissingItemException("You must have a ql 1 implant with at least one cluster equipped");
            }
        }

        internal Item HighestItem()
        {
            return DynelManager.LocalPlayer.GetStat(Stat.BreakingEntry) < 100 ? LowTradeskillItem : HighTradeskillItem;
        }

        internal bool FindImplant(out Item implant)
        {
            implant = Inventory.Items.FirstOrDefault(x => x.Slot.Type == IdentityType.ImplantPage && x.QualityLevel == 1);

            return implant != null;
        }

        internal bool FindNCU(out Item ncu)
        {
            ncu = Inventory.FindAll(36779).FirstOrDefault(x => x.Slot.Type == IdentityType.WeaponPage && x.QualityLevel == 1);

            return ncu != null;
        }

        internal bool FindImplantDisassemblyClinic(out Item implantDisassemblyClinic)
        {
            return Inventory.Find(161866, 161867, out implantDisassemblyClinic) || Inventory.Find(161867, 161867, out implantDisassemblyClinic); 
        }

        internal bool FindLockPick(out Item lockPick)
        {
            return Inventory.Find(95577, 95577, out lockPick);
        }

        internal bool FindHackerTool(out Item hackerTool)
        {
            return Inventory.Find(87810, 87814, out hackerTool) || Inventory.Find(87814, 87814, out hackerTool);
        }

        internal bool FindGrafts(out Item lowTradeskillItem, out Item highTradeskillItem)
        {
            bool foundLow = Inventory.Find(93734, 127497, out lowTradeskillItem) && lowTradeskillItem.Slot.Type == IdentityType.ArmorPage;
            bool foundHigh = Inventory.Find(128163, 128163, out highTradeskillItem) && highTradeskillItem.Slot.Type == IdentityType.ArmorPage;

            return foundLow || foundHigh;
        }
    }

    internal class MissingItemException : Exception
    {
        internal MissingItemException(string message) : base(message) { }
    }

}
