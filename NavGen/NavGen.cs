﻿using AOSharp.Common;
using AOSharp.Common.GameData;
using AOSharp.Common.Unmanaged.DbObjects;
using AOSharp.Common.Unmanaged.Imports;
using AOSharp.Common.Unmanaged.Interfaces;
using AOSharp.Core;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using AOSharp.Recast;
using org.critterai.geom;
using org.critterai.nav;
using org.critterai.nmbuild;
using org.critterai.nmgen;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NavGen
{
    public class NavGen : AOPluginEntry
    {
        public static Config Config;
        public static List<OffMeshLink> OffMeshLinks = new List<OffMeshLink>();
        private NavGenWindow _window;
        private Vector3 _testDestination = Vector3.Zero;
        private List<Vector3> _drawPath = new List<Vector3>();
        private PathCorridor _pathCorridor;

        public override void Run(string pluginDir)
        {
            Config = Config.Load();
            _window = new NavGenWindow(pluginDir);

            Chat.RegisterCommand("navgen", (command, param, chatWindow) =>
            {
                _window.Show();
            });

            Chat.RegisterCommand("addlink", (command, param, chatWindow) =>
            {
                Vector3 mouseWorldPos = Vector3.Zero;
                InputConfig_t.GetMouseWorldPosition(InputConfig_t.GetInstance(), ref mouseWorldPos);

                OffMeshLinks.Add(new OffMeshLink
                {
                    Start = DynelManager.LocalPlayer.Position,
                    End = mouseWorldPos
                });

                chatWindow.WriteLine($"Added link from {DynelManager.LocalPlayer.Position} to {mouseWorldPos}");
            });

            Chat.RegisterCommand("settestspot", (command, param, chatWindow) =>
            {
                _testDestination = DynelManager.LocalPlayer.Position;
            });

            Chat.RegisterCommand("testnav", (command, param, chatWindow) =>
            {
                Navmesh navmesh = _window.GetNavmesh();

                if(navmesh == null)
                {
                    chatWindow.WriteLine("No navmesh to test");
                    return;
                }

                NewNavmeshMovementController movementController = new NewNavmeshMovementController(true);
                MovementController.Set(movementController);
                movementController.SetNavmesh(navmesh);
                movementController.SetNavMeshDestination(_testDestination);
            });

            Chat.RegisterCommand("testnavdraw", (command, param, chatWindow) =>
            {
                Navmesh navmesh = _window.GetNavmesh();

                if (navmesh == null)
                {
                    chatWindow.WriteLine("No navmesh to test");
                    return;
                }

                NewNavmeshMovementController movementController = new NewNavmeshMovementController(true);
                MovementController.Set(movementController);
                movementController.SetNavmesh(navmesh);
                _drawPath = movementController.Pathfinder.GeneratePath(DynelManager.LocalPlayer.Position, _testDestination);
            });

            Chat.RegisterCommand("testnavdraw2", (command, param, chatWindow) =>
            {
                Navmesh navmesh = _window.GetNavmesh();

                if (navmesh == null)
                {
                    chatWindow.WriteLine("No navmesh to test");
                    return;
                }

                NewNavmeshMovementController movementController = new NewNavmeshMovementController(true);
                MovementController.Set(movementController);
                movementController.SetNavmesh(navmesh);
                _pathCorridor = movementController.Pathfinder.GeneratePathCorridor(DynelManager.LocalPlayer.Position, _testDestination);
            });

            Chat.RegisterCommand("cleardraw", (command, param, chatWindow) =>
            {
                _drawPath.Clear();
                _pathCorridor = null;
            });

            Game.OnUpdate += Update;
        }

        private void Update(object sender, float e)
        {
            _window.OnUpdate(sender, e);

            DrawTestPath();
            DrawTestPathCorridor();
        }

        internal void DrawTestPath()
        {
            if (!_drawPath.Any())
                return;

            Vector3 lastWaypoint = DynelManager.LocalPlayer.Position;
            foreach (Vector3 waypoint in _drawPath)
            {
                Debug.DrawLine(lastWaypoint, waypoint, DebuggingColor.Yellow);
                lastWaypoint = waypoint;
            }
        }

        internal void DrawTestPathCorridor()
        {
            if (_pathCorridor == null)
                return;

            int i = 0;
            Vector3 lastWaypoint = DynelManager.LocalPlayer.Position;
            foreach (org.critterai.Vector3 vert in _pathCorridor.Corners.verts)
            {
                Vector3 pos = vert.ToAOVector3();
                Debug.DrawLine(lastWaypoint, pos, DebuggingColor.Purple);
                lastWaypoint = pos;

                if (++i == _pathCorridor.Corners.cornerCount)
                    break;
            }
        }
    }

    public class OffMeshLink
    {
        public Vector3 Start;
        public Vector3 End;
    }
}
