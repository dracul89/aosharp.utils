﻿using AOSharp.Common.Helpers;
using AOSharp.Common.Unmanaged.Imports;
using System;
using System.Runtime.InteropServices;

namespace Neko
{
    public class DeathPatcher
    {
        private const string CharacterActionDeathCaseSig = "8B 83 ? ? ? ? 6A 10 8D 8B ? ? ? ? FF 50 18 8B 45 14 8B 40 04 8B 93 ? ? ? ? 50 68 ? ? ? ? 8D 8B ? ? ? ? FF 52 44 6A 01 8B CB E8 ? ? ? ? E9 ? ? ? ? 8B 45 14 FF 70 04 8B CB E8 ? ? ? ? E9 ? ? ? ? 8B 76 04 8B 8B ? ? ? ? 56 E8 ? ? ? ? E9 ? ? ? ? 8B 8B ? ? ? ? E8 ? ? ? ? E9 ? ? ? ? 8B 8B ? ? ? ? 6A 01 83 C3 14 53 E8 ? ? ? ? E9";

        private const int Offset = 0;
        private const int BlockSize = 6;
        private const byte NOP = 0x90;

        private static bool _patched = false;

        private static IntPtr _pCharacterActionDeathCase;
        private static IntPtr _pOrig;

        public static unsafe bool Patch()
        {
            IntPtr pCharacterActionDeathCase = Utils.FindPattern("Gamecode.dll", CharacterActionDeathCaseSig);

            if (pCharacterActionDeathCase == IntPtr.Zero)
                return false;

            if (!Kernel32.VirtualProtectEx(Kernel32.GetCurrentProcess(), pCharacterActionDeathCase + Offset, (UIntPtr)BlockSize, 0x40 /* EXECUTE_READWRITE */, out uint _))
                return false;

            _pOrig = Marshal.AllocHGlobal(BlockSize);

            Kernel32.CopyMemory(_pOrig, pCharacterActionDeathCase + Offset, BlockSize);

            byte[] patch = { 0xE9, 0xA0, 0x15, 0x00, 0x00, NOP };

            for (int i = 0; i < BlockSize; i++)
                *(byte*)(pCharacterActionDeathCase + Offset + i) = patch[i];

            _pCharacterActionDeathCase = pCharacterActionDeathCase;

            _patched = true;

            return true;
        }

        public static unsafe void Unpatch()
        {
            if (!_patched)
                return;

            Kernel32.CopyMemory(_pCharacterActionDeathCase + Offset, _pOrig, BlockSize);
            Marshal.FreeHGlobal(_pOrig);
            Kernel32.VirtualProtectEx(Kernel32.GetCurrentProcess(), _pCharacterActionDeathCase + Offset, (UIntPtr)BlockSize, 0x20 /* EXECUTE_READ */, out uint _);
            _patched = false;
        }
    }
}
